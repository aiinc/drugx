# Copyright 2015, 2016 Aaron Wise, Murat Can Cobanoglu
# 
# This file is part of DrugX.
# 
# DrugX is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# DrugX is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with DrugX.  If not, see <http://www.gnu.org/licenses/>.


import unittest
from drug_disease import DiDrGraph
import cPickle as pickle

class DIDRTest(unittest.TestCase):
  def test_stitch_edges(self):
    try:
      with open('didr_001.pickle', 'rb') as f:
        didr = pickle.load(f)
    except IOError:
        didr = DiDrGraph()
        with open('didr_001.pickle', 'wb') as f:
          pickle.dump(didr, f)
    self.assertEqual(didr.g_drug['CID009892644']['ENSG00000120907']['weight'], 690)
    self.assertEqual(didr.g_drug['CID009892644']['ENSG00000120907']['sign'], 0)
    self.assertEqual(didr.g_drug['CID044401952']['ENSG00000168748']['weight'], 402)
    self.assertEqual(didr.g_drug['CID044401952']['ENSG00000168748']['sign'], 0)
    self.assertEqual(didr.g_drug['CID100025892']['ENSG00000160868']['weight'], 900)
    self.assertEqual(didr.g_drug['CID100025892']['ENSG00000160868']['sign'], 1)

  def test_determine_sign(self):
    try:
      with open('didr_001.pickle', 'rb') as f:
        didr = pickle.load(f)
    except IOError:
        didr = DiDrGraph()
        with open('didr_001.pickle', 'wb') as f:
          pickle.dump(didr, f)
    self.assertEqual(didr.determine_sign(1, -1), 1)
    self.assertEqual(didr.determine_sign(1, 0), 1)
    self.assertEqual(didr.determine_sign(1, 1), -1)


if __name__ == '__main__':
  unittest.main()