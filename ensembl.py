# Copyright 2015, 2016 Aaron Wise, Murat Can Cobanoglu
# 
# This file is part of DrugX.
# 
# DrugX is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# DrugX is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with DrugX.  If not, see <http://www.gnu.org/licenses/>.


import csv

###
#returns a map from ensembl id to gene symbol and a map from gene symbol to ensembl id
###
def build_ensembl_maps(path=''):
  f_name = path+'ens2gene.txt'
  gene2ens = {}
  ens2gene = {}
  entrez2ens = {}
  with open(f_name, 'rb') as f:
    c = csv.reader(f)
    c.next()
    for line in c:
      if line[1] is not '':
        gene2ens[line[1]] = line[0]
        ens2gene[line[0]] = line[1]
      if line[2] is not '':
        entrez2ens[line[2]] = line[0]
  return (ens2gene, gene2ens, entrez2ens)

def build_ensp_ensg(path=''):
  f_name = path+'ensgensp.txt'
  ensp2ensg = {}
  with open(f_name, 'rb') as f:
    c = csv.reader(f)
    c.next()
    for line in c:
      if line[0] is not '' and line[1] is not '':
        ensp2ensg[line[1]] = line[0]
  return ensp2ensg
