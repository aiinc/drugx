# Copyright 2015, 2016 Aaron Wise, Murat Can Cobanoglu
# 
# This file is part of DrugX.
# 
# DrugX is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# DrugX is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with DrugX.  If not, see <http://www.gnu.org/licenses/>.


###
# Home for various statistical tests for our tcga data.
###

import sys
import math
import csv
import logging
import matplotlib
import requests
from StringIO import StringIO
from xml.etree import ElementTree
matplotlib.use('Agg')
from matplotlib import pyplot as plot
import numpy
import cProfile, pstats, StringIO
from scipy import stats
from pprint import pprint
import pandas as pd
import joblib
import cPickle as pickle
import os.path
import networkx as nx
import params_parser
import ensembl
from collections import namedtuple

AnalysisTuple = namedtuple('AnalysisTuple', ['gene_id', 'statistic', 'pval', 'mean_diff'])


def get_KEGG_map():
  return dict([(l.split('\t')[1].split(':')[1],l.split('\t')[0]) for l in file('hsa_ensembl.list')])

def get_features(prot_id, ens2hsa):
  pathways = []
  pfam = []
  definition = ''
  if prot_id in ens2hsa:
    prot_id = ens2hsa[prot_id]
    r = requests.get('http://rest.kegg.jp/get/'+prot_id)
    for l in r.text.split('\n'):
      k = l[:12].strip(); v = l[12:]
      if len(l[:12]): 
        categ = k
      if categ == "PATHWAY":
        pathways.append(l[22:].strip())
      elif categ == "DEFINITION":
        definition = l[12:]
    r = requests.get('http://rest.genome.jp/link/'+prot_id+'/original')
    links = pd.read_csv(StringIO(r.text),'\t',header=None,names=['Protein','Link','Type'])['Link']
    for pf in [s.split(':')[1] for s in links[links.apply(lambda x:x.startswith('pf:'))]]:
      pfam_r = requests.get('http://pfam.xfam.org/family/'+pf+'?output=xml')
      et = ElementTree.fromstring(pfam_r.text)
      pfam.append(et[0][0].text.strip())
  return {'pathways':set(pathways),
      'pfam':set(pfam),
      'definition':definition}

def get_tissue_expressions(f='rna_transcript.tsv'):
  df = pd.read_csv(f,'\t')
  df_g = df.groupby('ensgid')
  return df_g

def get_tissues(gene_id, tissue_expression, FPKM_THRESHOLD=1):
  """VERY CRUDE analysis of which tissues express this protein
  *gene_id*: ENSG identifier
  *tissue_expression*: tissue expression dataframe grouped by genes [i.e. as returned by get_tissue_expressions()]
  *FPKM_threshold*: tissues expressing the gene above this value will be returned"""
  df_g = tissue_expression
  tissues = set()
  if gene_id in df_g.groups:
    g = df_g.get_group(gene_id)
    rows = g[g.transcript_biotype=='protein_coding']
    if len(rows)>0:
      exp = rows.iloc[0,4:]
      tissues = set([s.split('.')[1].split('_')[0].title() for s in exp[exp>FPKM_THRESHOLD].index])
  return tissues

def get_target_report(disease, sorted_tuples, n=50):
  data = {'ID':[],
  'NAME':[],
  'DEFN':[], 
  'PVAL':[],
  'PFAM':[],
  'TISSUE':[],
  'PATHWAYS':[] }

  logging.info('Preparing target report')
  ens2hsa = get_KEGG_map()
  df_g = get_tissue_expressions()
  for t in sorted_tuples[:n]:
    logging.debug(t.gene_id)
  data['ID'].append(t.gene_id)
  data['NAME'].append(ens2gene[t.gene_id])
  res = getFeatures(t.gene_id, ens2hsa)
  data['DEFN'].append(res['definition'])
  data['PATHWAYS'].append(';'.join(res['pathways']))
  data['PFAM'].append('\n'.join(res['pfam']))
  data['PVAL'].append(t.pval)
  data['TISSUE'].append(', '.join(getTissues(t.gene_id, df_h)))
  data = pd.DataFrame(data)
  data[['ID','NAME','DEFN','PVAL','PFAM','PATHWAYS','TISSUE']].to_excel(pd.ExcelWriter(disease+'_picks.xlsx'))

###
# Validated: whether we only return validated edges
# TODO: map from ENSP to ENSG instead of using gene symbol
###
def load_known_cancer_genes(path='', validated=True):
  (ens2gene, gene2ens, entrez2ens) = ensembl.build_ensembl_maps(path)
  with open(path+'cancergenes.txt', 'rb') as f:
    c = csv.reader(f, delimiter='\t')
    header = c.next()
    if validated == True:
      genes = [gene2ens[line[1]] for line in c if line[1] in gene2ens and line[10] == "known"]
    else:
      genes = [gene2ens[line[1]] for line in c if line[1] in gene2ens]
    return set(genes)

def load_humannet(path=''):
  g=nx.Graph()
  (ens2gene, gene2ens, entrez2ens) = ensembl.build_ensembl_maps(path)
  with open(path+'HumanNet.v1.join.txt', 'rb') as f:
    c = csv.reader(f, delimiter='\t')
    for line in c:
      try:
        t = [entrez2ens[line[0]], entrez2ens[line[1]], line[-1]]
        g.add_edge(t[0], t[1], weight=t[2])
      except KeyError:
        continue
  return g

###
# Function that is called to run a per-gene t-test given two samples (represented by queries on the datastore)
###
def gene_utest_tcga_himem(identifier, df, q_a, q_b, path="", regenerate=False):
  filename = path+identifier+'.pickle'
  if os.path.isfile(filename) and not regenerate:
    try:
      with open(filename, 'rb') as f:
        sorted_tuples = pickle.load(f)
    except IOError:
      regenerate = True
  elif not(os.path.isfile(filename)) and not regenerate:
    regenerate = True
  if regenerate:
    score_tuples = []
    df_a = df[q_a]
    df_b = df[q_b]
    g_a = df_a.groupby('gene_id')
    g_b = df_b.groupby('gene_id')
    for name, group_a in g_a:
      group_b = g_b.get_group(name)
      scores_a = group_a['normalized_count']; mean_a = numpy.mean(scores_a);
      scores_b = group_b['normalized_count']; mean_b = numpy.mean(scores_b);
      logging.debug("Name: "+name)
      logging.debug("Group A mean: "+str(mean_a) + \
                   " Group B mean: "+str(mean_b))
      if (mean_a == mean_b) and (numpy.std(scores_a) == numpy.std(scores_b)) and (numpy.std(scores_a)==0):
        logging.info('Gene '+name+' has exactly the same mean and 0 standard deviation '+\
            'for expression values in both cancer and normal with mean: ' +str(mean_a))
        score_tuples.append(AnalysisTuple(name, None, 1, 
          mean_diff=numpy.mean(group_a['normalized_count']) - numpy.mean(group_b['normalized_count'])))
      else:
        score_tuples.append(AnalysisTuple(name, *stats.mannwhitneyu(group_a['normalized_count'], group_b['normalized_count']),
          mean_diff=numpy.mean(group_a['normalized_count']) - numpy.mean(group_b['normalized_count'])))
    sorted_tuples = sorted(score_tuples, key=lambda x: x.pval)
    with open(filename, 'wb') as f:
      pickle.dump(sorted_tuples, f)
  plot_test_dist(identifier, sorted_tuples, path)
  return sorted_tuples


###
# @deprecated
# Function that is called to run a per-gene t-test given two samples (represented by queries on the datastore)
###
def gene_ttest_tcga(identifier, df, q_a, q_b, path="", regenerate=False):
  filename = path+identifier+'.pickle'
  if os.path.isfile(filename) and not regenerate:
    with open(filename, 'rb') as f:
      sorted_tuples = pickle.load(f)
  else:
    genes = all_genes(df)
    df.close()
    score_tuples = joblib.Parallel(n_jobs=12)(joblib.delayed(two_sample_test)(g,
          q_a + ' & gene_id == ' + g,
          q_b + ' & gene_id == ' + g,
          path=path)
          for g in genes)
    sorted_tuples = sorted(score_tuples, key=lambda x: x[1])
    with open(filename, 'wb') as f:
      pickle.dump(sorted_tuples, f)
  #plot_test_dist(identifier, sorted_tuples)#TEMP!
  return sorted_tuples

###
# @deprecated
# Given our datastore, returns a list of all known genes
###
def all_genes(store):
  ids = store.select_column('data', 'gene_id')
  return ids.unique()


###
#@deprecated
###
def two_sample_test(identifier, query1, query2, path=""):
  store = pd.HDFStore(path+'store.h5', mode='r')
  data_a = store.select('data', query1, columns=['normalized_count'])
  data_b = store.select('data', query2, columns=['normalized_count'])
  store.close()
  return (identifier, stats.ttest_ind(data_a['normalized_count'], data_b['normalized_count'], equal_var=False))

###
# Plots the distribution of all t_test p-values
###
def plot_test_dist(identifier, sorted_tuples, path=''): #path is necessary for load_known_cancer_genes
  print identifier
  print sorted_tuples[:10]
  results = [x.pval for x in sorted_tuples]
  #print "all genes: "+str(len(results))
  results = [x for x in results if not math.isnan(x)]
  #print "not nan: "+str(len(results))
  plot.hist(results, bins=numpy.logspace(-100, 0, 50), alpha=0.7)
  plot.gca().set_xscale("log")
  cancergenes = load_known_cancer_genes(path)
  cancer_results = [x.pval for x in sorted_tuples if x.gene_id in cancergenes]
  weight = float(len(results))/float(len(cancer_results))
  plot.hist(cancer_results, bins=numpy.logspace(-100, 0, 50), alpha=0.7, weights = [weight]*len(cancer_results))
  plot.legend(['Input','Known'],loc=2)
  plot.title('P-Values of Input Genes vs Known Cancer Genes')
  plot.savefig(identifier+'.png')
  not_cancer_results = [x.pval for x in sorted_tuples if x.gene_id not in cancergenes]
  print "cancer gene u-test: " + identifier + " "+str(stats.mannwhitneyu(cancer_results, not_cancer_results))
  plot.clf()

###
# Builds the graph that is then used by drug_disease.py to link the drug and disease graphs.
# This is currently version 0.0000001 -- we're using very few diseases, and a SUPER naive way of analysis.
# Also should look at effect size, and differences in p-value due to data set sizes
# TODO: diseases and significance thresholds are currently set by hand.
###
def build_disease_graph(params):
  df = pd.read_hdf(params.datadir+params.store_file, 'data')
  all_diseases = params.cancer_list
  cutoffs = params.threshold_list
  all_genes = set()
  top_links = []
  for (i,disease) in enumerate(all_diseases):
    disease_tuples = gene_utest_tcga_himem('ut_'+disease,
                                           df,
                                           (df['cancer_type'] == disease) & (df['tissue_type'] == 'normal'),
                                           (df['cancer_type'] == disease) & (df['tissue_type'] == 'cancer'),
                                           path=params.datadir)
    #sign(mean_diff) tells you if the expression has increased or decreased in the cancer cells
    #thus, for our links, positive sign = upregulated and negative sign = downregulated
    disease_links = [(d_tuple.gene_id, disease, -numpy.log10(d_tuple.pval), numpy.sign(d_tuple.mean_diff))
                     for d_tuple in disease_tuples
                     if numpy.log10(d_tuple.pval) < cutoffs[i]]
    top_links += disease_links
    print "Disease "+disease+" sig gene count: "+str(len(disease_links))
  g = nx.Graph()
  for (gene, disease, weight, sign) in top_links:
    g.add_edge(disease, gene, weight=weight, sign=sign)
    all_genes.add(gene)
  return (g, all_genes, all_diseases)


if __name__ == '__main__':
  #g = load_humannet()
  #exit()
  # STEP 2: let's do some biology!
  param_in = sys.argv[1]
  params = params_parser.parse_yaml(param_in)
  logging.basicConfig(level=params.log_level,
          filename=params.identifier+'.log',
          format='%(asctime)s %(name)-12s %(levelname)-8s %(message)s',
          datefmt='%m-%d %H:%M:%S')
  df = pd.read_hdf(params.datadir+params.store_file, 'data')
  for disease in params.cancer_list:
    logging.debug("DISEASE: "+disease)
    df_a = (df['cancer_type'] == disease) & (df['tissue_type'] == 'normal')
    df_b = (df['cancer_type'] == disease) & (df['tissue_type'] == 'cancer')
#    if len(df[df_a])==0:
#      df_a = (df['tissue_type'] == 'normal')
    gene_utest_tcga_himem('ut_'+disease, df, df_a, df_b, path=params.datadir)
  #gene_utest_tcga_himem('tt_coad', df, (df['cancer_type'] == 'coad') & (df['tissue_type'] == 'normal'), (df['cancer_type'] == 'coad') & (df['tissue_type'] == 'cancer'))
  #gene_utest_tcga_himem('tt_esca', df, (df['cancer_type'] == 'esca') & (df['tissue_type'] == 'normal'), (df['cancer_type'] == 'esca') & (df['tissue_type'] == 'cancer'))
  #gene_utest_tcga_himem('tt_coad_esca_cancer', df, (df['cancer_type'] == 'coad') & (df['tissue_type'] == 'cancer'), (df['cancer_type'] == 'esca') & (df['tissue_type'] == 'cancer'))
  #gene_utest_tcga_himem('tt_coad_esca_normal', df, (df['cancer_type'] == 'coad') & (df['tissue_type'] == 'normal'), (df['cancer_type'] == 'esca') & (df['tissue_type'] == 'normal'))
  #gene_utest_tcga_himem('tt_hnsc', df, (df['cancer_type'] == 'hnsc') & (df['tissue_type'] == 'normal'), (df['cancer_type'] == 'hnsc') & (df['tissue_type'] == 'cancer'))
  #gene_utest_tcga_himem('tt_coad_hnsc_cancer', df, (df['cancer_type'] == 'coad') & (df['tissue_type'] == 'cancer'), (df['cancer_type'] == 'hnsc') & (df['tissue_type'] == 'cancer'))
  #gene_utest_tcga_himem('tt_coad_hnsc_normal', df, (df['cancer_type'] == 'coad') & (df['tissue_type'] == 'normal'), (df['cancer_type'] == 'hnsc') & (df['tissue_type'] == 'normal'))
  #gene_utest_tcga_himem('tt_lihc', df, (df['cancer_type'] == 'lihc') & (df['tissue_type'] == 'normal'), (df['cancer_type'] == 'lihc') & (df['tissue_type'] == 'cancer'))
  #gene_utest_tcga_himem('tt_prad', df, (df['cancer_type'] == 'prad') & (df['tissue_type'] == 'normal'), (df['cancer_type'] == 'prad') & (df['tissue_type'] == 'cancer'))
