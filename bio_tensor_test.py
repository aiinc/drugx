# Copyright 2015, 2016 Aaron Wise, Murat Can Cobanoglu
# 
# This file is part of DrugX.
# 
# DrugX is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# DrugX is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with DrugX.  If not, see <http://www.gnu.org/licenses/>.


import unittest
from crossdrug_tensor import CDTensor
from bio_tensor import *
import tensor_evaluate
import heapq
import params_parser
from sktensor import sptensor, cp_als

class BioTensorTest(unittest.TestCase):

  ###
  #Build a tensor for us to run tests on.
  ###
  @classmethod
  def setUpClass(cls):
    cls.p = params.parse_yaml('tensor_test.yaml')
    TE = tensor_evaluate.TensorEvaluator(cls.p)
    (train_set, test_set, drugs, diseases, genes) = TE.s3_cv_tensor()
    cls.train_set = train_set
    cls.test_set = test_set
    cls.t = CDTensor(train_set, drugs, diseases, genes, cls.p)

  ###
  # Makes sure all the train data is still at 1.0 after adding drug inference
  ###
  def testCDTensor(self):
    t = self.t
    for datum in self.train_set:
      datum = t.as_index_entries_genes([datum])
      for entry in datum:
        self.assertEqual(t.entries[entry], 1.0)

  def testUpdateHeapq(self):
    t = self.t
    old_known_hits = t.known_hits
    t.known_hits = set([(6,1,1)])
    hq = [(0.0, (0,0,0)) for i in range(5)]
    current_worst = 0.0
    scores = numpy.array([0.1, -0.5, -0.3, 1.0, 2.0, 2.0, 3.0])
    #indices = numpy.array([(1,1,1), (2,1,1), (3,1,1), (4,1,1), (5,1,1), (6,1,1), (7,1,1)])
    indices = numpy.array([[1,1,1], [2,1,1], [3,1,1], [4,1,1], [5,1,1], [6,1,1], [7,1,1]])
    order = numpy.argsort(scores)
    scores = scores[order]
    indices = indices[order]
    (current_worst, hq) = t.update_heapq(current_worst, hq, scores, indices)
    self.assertEqual(current_worst, 0.0)
    scores = numpy.array([0.1, -0.5, -0.3, 1.0, 4.0, 3.0])
    indices = numpy.array([(11,1,1), (12,1,1), (13,1,1), (14,1,1), (15,1,1), (16,1,1)])
    order = numpy.argsort(scores)
    scores = scores[order]
    indices = indices[order]
    (current_worst, hq) = t.update_heapq(current_worst, hq, scores, indices)
    self.assertEqual(current_worst, 1.0)
    hq = sorted(hq, key = lambda x: x[0])
    self.assertEqual(hq[0][0], 1.0)
    self.assertEqual(hq[-1][0], 4.0)
    t.known_hits = old_known_hits

  ###
  # Test that our jit_predict_v2 method of reconstituting the tensor gives the same result as the scikit-tensor version.
  ###
  def testTensor(self):
    a = numpy.random.randint(2, size=7)
    b = numpy.random.randint(2, size=7)
    c = numpy.random.randint(2, size=7)
    sp = sptensor(tuple([a,b,c]), [1]*7, shape=(10,10,10))
    P, fit, itr, exectimes = cp_als(sp, 3, init='random')
    sktensor_solution = P.totensor()
    scores = numpy.zeros(1000)
    indices = numpy.zeros((1000, 3), dtype=int)
    (num_assigned, indices, scores) = jit_predict_v2(scores, indices, P.lmbda, P.U[0], P.U[1], P.U[2], 0, 10, 0, 10, 10)
    scores = scores[:num_assigned]
    indices = indices[:num_assigned]
    for (i,x) in enumerate(scores):
      index = indices[i]
      self.assertAlmostEqual(sktensor_solution[index[0], index[1], index[2]], x)

  ###
  #check that there's no train/test pollution
  ###
  def testTrainTestPollution1(self):
    formatted_train_set = self.t.as_index_entries_genes(self.train_set)
    formatted_train_set = self.t.as_name_entries(formatted_train_set)
    formatted_test_set = self.t.as_index_entries_genes(self.test_set)
    formatted_test_set = self.t.as_name_entries(formatted_test_set)
    test_train_overlap = set(formatted_train_set).intersection(set(formatted_test_set))
    self.assertEqual(len(test_train_overlap), 0)

  def testTrainTestPollution2(self):
    formatted_train_set = self.t.as_index_entries_genes(self.train_set)
    formatted_train_set = self.t.as_name_entries(formatted_train_set)
    test_ranks = self.t.chunked_make_predictions(indices_to_rank=self.t.as_index_entries_genes(self.test_set), n=5000)
    predictions = set([self.t.as_name_entry(i,j,k) for (score, (i,j,k)) in self.t.current_predictions])
    predictions_in_train = set(formatted_train_set).intersection(set(predictions))
    self.assertEqual(len(predictions_in_train),0)

  ###
  # Tests that our batch_predict and single_predict give the same results
  ###
  def testTensorScoreMethods(self):
    t = self.t
    for index in [(0,0,0), (1,50,250), (1,150,20), (1,111,20), (0,222,20), (2,450,200), (3,350,720)]:
      score_single = t.single_predict(*index)
      scores = numpy.zeros(len(t.genes))
      indices = numpy.zeros((len(t.genes), 3), dtype=int)
      (num_assigned, indices, scores) = jit_predict_v2(scores, indices, t.P.lmbda, t.P.U[0], t.P.U[1], t.P.U[2], index[0], index[0]+1,
                                             index[1], index[1]+1, len(t.genes))
      score_mult = scores[index[2]]
      print indices
      print scores
      if not tuple(indices[index[2]]) == (0,0,0):
        self.assertEqual(index, tuple(indices[index[2]]))
        self.assertEqual(score_single, score_mult)
      else:
        self.assertTrue(score_single <= 0)



  #check that the best test element is in test_ranks
  #TODO
  #test subset_test_set
  #test that indices remain consistent.


if __name__ == '__main__':
  unittest.main()
