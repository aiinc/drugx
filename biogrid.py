# Copyright 2015, 2016 Aaron Wise, Murat Can Cobanoglu
# 
# This file is part of DrugX.
# 
# DrugX is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# DrugX is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with DrugX.  If not, see <http://www.gnu.org/licenses/>.


import csv
import ensembl

###
#Here we create a ppi-network from biogrid, expressed
#as a map from ensembl ids to a set of ensembl ids.
###
def parse_biogrid():
  biogrid_map = {}
  f_name = 'BIOGRID-ORGANISM-Homo_sapiens-3.2.117.tab2.txt'
  (ens2gene, gene2ens, entrez2ens) = ensembl.build_ensembl_maps()
  with open(f_name) as f:
    c = csv.reader(f, delimiter='\t')
    print c.next()
    for line in c:
      if line[1] is not '':
        try:
          ens_a = entrez2ens[line[1]]
        except KeyError:
          print "entrez id not found "+line[1]
          continue
      else:
        ens_a = gene2ens[line[7]]
      if line[2] is not '':
        try:
          ens_b = entrez2ens[line[2]]
        except KeyError:
          print "entrez id not found "+line[2]
          continue
      else:
        ens_b = gene2ens[line[8]]
      if ens_a in biogrid_map:
        biogrid_map[ens_a].add(ens_b)
      else:
        biogrid_map[ens_a] = set()
        biogrid_map[ens_a].add(ens_b)
      if ens_b in biogrid_map:
        biogrid_map[ens_b].add(ens_a)
      else:
        biogrid_map[ens_b] = set()
        biogrid_map[ens_b].add(ens_a)
  return biogrid_map


if __name__ == '__main__':
  biogrid_map = parse_biogrid()
  print biogrid_map