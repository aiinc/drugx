#!/usr/bin/python
import sys,os,shutil
import argparse

parser = argparse.ArgumentParser(description='Separate tensor pickles files to appropriate folders')
parser.add_argument('ident_str',
                    default='',
                    help='Identification string, only filenames that contain this string are selected')
parser.add_argument('-t','--target_dir',
                    default=None,
                    help='The target directory into which files will be placed',
                    required = False)
parser.add_argument('-v', '--verbose',
                    action='store_true',
                    help='Verbose mode outputs the moves')
ns = parser.parse_args()

flist = [f for f in os.listdir(os.getcwd()) \
            if ns.ident_str in f \
            and f.endswith('trr.pickle')\
            and os.path.isfile(f)]
if not(os.path.isdir(ns.target_dir)):
  os.mkdir(ns.target_dir)
for f in flist:
  f_type = f.split('_')[0]
  if not(os.path.isdir(ns.target_dir+os.sep+f_type)):
    os.mkdir(ns.target_dir+os.sep+f_type)
  if ns.verbose:
    print f, '-->', ns.target_dir+os.sep+f_type+os.sep+f
  shutil.move(f,ns.target_dir+os.sep+f_type+os.sep+f)
