# Copyright 2015, 2016 Aaron Wise, Murat Can Cobanoglu
# 
# This file is part of DrugX.
# 
# DrugX is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# DrugX is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with DrugX.  If not, see <http://www.gnu.org/licenses/>.


import cPickle as pickle

class TensorRunnerRecord(object):

  ###
  # tr: TensorRunner instance to record
  ###
  def __init__(self, tr):
    self.n_predictions_made         = tr.n_predictions_made
    self.n_predictions_correct      = tr.n_predictions_correct
    self.n_predictions_stitch       = tr.n_predictions_stitch
    self.learning_factor            = tr.learning_factor
    self.cumulative_learning_factor = tr.cumulative_learning_factor
    self.baseline_discovery_rate    = tr.baseline_discovery_rate
    self.old_predictions            = tr.old_predictions
    self.all_old_predictions        = tr.all_old_predictions
    self.tensor_type                = tr.tensor_type
    self.params                     = tr.params

  ###
  # tr: TensorRunner instance to record
  ###
  @staticmethod
  def save(tr):
    trr = TensorRunnerRecord(tr)
    with open(tr.params.identifier+'_trr.pickle','wb') as f:
      pickle.dump(trr,f)

  ###
  # fname: file that contains pickle object to load
  ###
  @staticmethod
  def load(fname):
    with open(fname,'rb') as f:
      return pickle.load(f)
