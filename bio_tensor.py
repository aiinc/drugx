# Copyright 2015, 2016 Aaron Wise, Murat Can Cobanoglu
# 
# This file is part of DrugX.
# 
# DrugX is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# DrugX is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with DrugX.  If not, see <http://www.gnu.org/licenses/>.


###
# Our tensor wrapper class.
# Tensor dimensions are DISEASE x DRUG x GENE
###
from sktensor import sptensor, cp_als
from numba import jit
#import pyximport; pyximport.install()
#import tensor_predict
import numpy
import heapq
import logging

class BTensor(object):

  ###
  # We assert that drugs, diseases and genes are lists, as order matters -- we use these indices as
  # the map for our tensor dimensions.
  # entries: a map from (int-index) tuples to their weights (currently boolean)
  # known_hits: a set of (int-index) tuples containing all the 'verified' hits (that we do not want to propose as an experiment)
  # current_predictions: a list of (score, int-index) tuples
  ###
  def __init__(self, in_entries, drugs, diseases, genes, params):
    self.drugs = list(drugs)
    self.drugs_to_index = {x: i for (i,x) in enumerate(drugs)}
    self.diseases = list(diseases)
    self.disease_to_index = {x: i for (i,x) in enumerate(diseases)}
    self.genes = list(genes)
    self.genes_to_index = {x: i for (i,x) in enumerate(genes)}
    self.entries = {}
    self.known_hits = set()
    self.params = params
    self.add_data(in_entries)
    self.compute()
    self.current_predictions = []

  ###
  # At the moment, we hold an scikit-tensor sptensor inside our tensor wrapper.
  # This sptensor is used to run sktensor's decomposition algorithms.
  ###
  def build_sptensor(self):
    indices = zip(*self.entries.keys())
    indices = [numpy.array(x) for x in indices]
    indices = tuple(indices)
    logging.info("Tensor dims: "+str((len(self.diseases), len(self.drugs), len(self.genes))))
    self.sptensor = sptensor(indices, [1]*len(self.entries), shape=(len(self.diseases), len(self.drugs), len(self.genes)))

  ###
  # Currently, entries is boolean.
  ###
  def add_data(self, in_entries):
    for (disease, drug, genes, score) in in_entries:
      for gene in genes:
        self.entries[(self.disease_to_index[disease], self.drugs_to_index[drug], self.genes_to_index[gene])] = 1
    self.known_hits = self.known_hits.union(self.entries.keys())
    self.build_sptensor()

  ###
  # Given our input format, and indices, returns a set of entries containing the links found in entries
  # entries: iterable of disease, drug, genes (iterable), score
  # formatted_entries: tuples of (disease_index, drug_index, gene_index)
  ###
  def as_index_entries_genes(self, entries):
    formatted_entries = set()
    for (disease, drug, genes, score) in entries:
      for gene in genes:
        formatted_entries.add((self.disease_to_index[disease], self.drugs_to_index[drug], self.genes_to_index[gene]))
    return formatted_entries

  ###
  # entries: iterable of (disease, drug, gene) tuples
  # formatted_entries: tuples of (disease_index, drug_index, gene_index)
  ###
  def as_index_entries_gene(self, entries):
    formatted_entries = set()
    for (disease, drug, gene) in entries:
      formatted_entries.add((self.disease_to_index[disease], self.drugs_to_index[drug], self.genes_to_index[gene]))
    return formatted_entries

  ###
  #
  ###
  def as_name_entries(self, entries):
    formatted_entries = set()
    for (i_disease, i_drug, i_gene) in entries:
      formatted_entries.add(self.as_name_entry(i_disease, i_drug, i_gene))
    return formatted_entries

  def as_name_entry(self, i_disease, i_drug, i_gene):
    return (self.diseases[i_disease], self.drugs[i_drug], self.genes[i_gene])

  ###
  # Currently, we take the 5 best scores, and then randomly select 500.
  # indices_to_rank: list of index_tuples
  # index_scores: list of floats; the scores of the 5 best-scoring test elements, as well as 500 random ones
  ###
  def subset_test_set(self, indices_to_rank):
    index_scores = numpy.array([self.single_predict(*coord) for coord in indices_to_rank])
    if len(index_scores) > 0: # If there is no test data, we just return an empty array
      index_scores = numpy.concatenate([sorted(index_scores)[-5:], numpy.random.choice(index_scores, size=500)])
    return index_scores

  ###
  # Finds the top-n scoring predictions that aren't already known to be 1.
  # chunksize: how many drugs we calculate per sub-function call.  Proportional to memory usage
  ###
  def chunked_make_predictions(self, indices_to_rank=[], chunksize=50000, n=100):
    index_scores = self.subset_test_set(indices_to_rank)
    #worst_index_score = numpy.min(index_scores)
    index_rank = numpy.zeros(len(index_scores))
    hq = [(0.0, (0,0,0)) for i in range(n)]
    current_worst = 0.0
    chunkstarts = range(0, len(self.drugs), chunksize)
    num_values = chunksize*len(self.genes)
    for i in range(0, len(self.diseases)):
      for dr_min in chunkstarts:
        dr_max = numpy.min([len(self.drugs), dr_min+chunksize]) #either we stop with a full chunk, or at the end of the list
        # set up variables for jit function
        scores = numpy.zeros(num_values)
        indices = numpy.zeros((num_values, 3), dtype=int)
        logging.info("Beginning chunk")
        (num_assigned, indices, scores) = jit_predict_v2(scores, indices, self.P.lmbda, self.P.U[0], self.P.U[1], self.P.U[2], i, i+1,
                                           dr_min, dr_max, len(self.genes))
        scores = scores[:num_assigned]
        indices = indices[:num_assigned]
        logging.info("Sorting chunk results")
        order = numpy.argsort(scores)
        scores = scores[order]
        indices = indices[order]
        logging.info("Evaluating test set")
        index_rank = evaluate_test_set(index_rank, index_scores, scores)
        logging.info("maintaining top-n")
        (current_worst, hq) = self.update_heapq(current_worst, hq, scores, indices)
        logging.info("done with chunk "+str(i)+"."+str(dr_min))
        logging.debug(hq)
    self.current_predictions = sorted(hq, key=lambda x: x[0])
    #adjust index ranks for  training set
    train_scores = numpy.array([self.single_predict(*coord) for coord in self.known_hits])
    index_rank = adjust_train_set(index_rank, index_scores, train_scores)
    return index_rank
    #logging.debug(hq[:50])

  ###
  # Given a set of scores from the tensor, updates our heapq with the top
  # scoring entries.
  # invariant: scores is sorted.
  ###
  def update_heapq(self, current_worst, hq, scores, indices):
    for j in range(len(scores)-1, 0, -1):
            if scores[j] < current_worst:
              break
            elif not tuple(indices[j]) in self.known_hits:
              current_worst = heapq.heappushpop(hq, (scores[j], tuple(indices[j])))[0]
    return (current_worst, hq)

  ###
  # @deprecated in favor of chunked_make_predictions
  # Returns the top-n _most confident_ predictions
  ###
  def make_predictions(self, n=100):
    logging.info("Beginning to unpack kruskal operator")
    #results = heapq.nlargest(n, self.iter_predict(), lambda x: x[1])
    scores = numpy.zeros(n)
    indices = numpy.zeros((n,3))
    results = jit_predict(numpy.array(scores), numpy.array(indices), self.P.lmbda, self.P.U[0], self.P.U[1], self.P.U[2], len(self.diseases), len(self.drugs), len(self.genes), n=n)
    print results
    #results = tensor_predict.cy_predict(numpy.array(scores), numpy.array(indices), self.P.U[0], self.P.U[1], self.P.U[2], len(self.diseases), len(self.drugs), len(self.genes), n=n)
    logging.info("Done unpacking kruskal operator")
    return results

  ###
  # Given new data, recompute the tensor
  # new_entries are new (positive) training examples as name_entries
  # new_neg_entries are the experiments we ran that failed as index_entries
  ###
  def update(self, new_entries, new_neg_entries):
    new_entries = [(disease, drug, [gene], 1.0) for (disease, drug, gene) in new_entries]
    self.add_data(new_entries)
    #we add the null entries to the things we will not test, but do not
    #give them an entry in the tensor, since they are value 0
    self.known_hits = self.known_hits.union(new_neg_entries)
    if not(self.params.passive_mode):
      self.compute()

  ###
  # Using PTF, calculate the kruskal operator P
  ###
  def compute(self):
    logging.info("Beginning to run PARAFAC")
    logging.info("Using rank="+str(self.params.rank))
    numpy.random.seed(self.params.init_seed)
    P, fit, itr, exectimes = cp_als(self.sptensor, self.params.rank, init=self.params.cp_init, conv=self.params.conv_thr)
    logging.info("Done with PARAFAC")
    self.P = P

  ###
  # Iterator through the tensor returning the reconstructed values
  ###
  def iter_predict(self):
    count = 0
    for i in range(0, len(self.diseases)):
      for j in range(0, len(self.drugs)):
        if j % 1000 == 0:
          logging.info(str(count)+"T drugs")
          count += 1
          ij = self.P.U[0][i] * self.P.U[1][j]
          if numpy.max(numpy.abs(ij)) == 0:
            continue
        for k in range(0, len(self.genes)):
          score = numpy.sum(self.P.lmbda * ij * self.P.U[2][k])
          yield ((i,j,k), score)

  ###
  # Given a single triplet, returns its predicted value.
  ###
  def single_predict(self, i, j, k):
    return numpy.sum(self.P.lmbda * self.P.U[0][i] * self.P.U[1][j] * self.P.U[2][k])

###
# For each test index, adds the number of scores greater than it in this chunk.
# After all chunks, this will tell you the rank of that index.
###
#@jit(nopython=True)
def evaluate_test_set(index_rank, index_scores, scores):
  for (j, index_score) in enumerate(index_scores):
    index_rank[j] += numpy.sum(scores > index_score)
  return index_rank

###
# The exact opposite of evaluate_test_set.  Once we've found the ranks of
# everything, we want the ranks to exclude the training data, so we remove their ranks
###
#@jit(nopython=True)
def adjust_train_set(index_rank, index_scores, train_scores):
  for (j, index_score) in enumerate(index_scores):
    index_rank[j] -= numpy.sum(train_scores > index_score)
  return index_rank

###
# We recalculate each (nonzero) entry's value using the kruskal operators.
###
@jit(nopython=True)
def jit_predict_v2(scores, indices, lmbda, U0, U1, U2, dis_min, dis_max, dr_min, dr_max, n_g):
  num_assigned = 0
  for i in range(dis_min, dis_max):
    for j in range(dr_min, dr_max):
      ij = U0[i] * U1[j]
      ij = ij * lmbda
      if numpy.max(numpy.abs(ij)) == 0:
        continue
      for k in range(0, n_g):
        score = numpy.sum(ij * U2[k])
        if numpy.abs(score) > 0:
          scores[num_assigned] = score
          indices[num_assigned, 0] = i
          indices[num_assigned, 1] = j
          indices[num_assigned, 2] = k
          num_assigned += 1
  return (num_assigned, indices, scores)

###
# @deprecated
###
@jit(nopython=True)
def jit_predict(scores, indices, lmbda, U0, U1, U2, dis_min, dis_max, dr_min, dr_max, n_g, n=100):
  current_worst = 0.0
  for i in range(dis_min, dis_max):
    for j in range(dr_min, dr_max):
      if j % 10000 == 0:
        print(j)
      ij = U0[i] * U1[j]
      if numpy.max(numpy.abs(ij)) == 0:
        continue
      for k in range(0, n_g):
        score = numpy.sum(lmbda * ij * U2[k])
        if score > current_worst:
          index = (i,j,k)
          for i in range(n-1, 0, -1):
            if score > scores[i]:
              scores = numba_bubble(i, score, scores)
              indices = numba_bubble_2d(i, index, indices)
              current_worst = scores[0]
  return (indices, scores)

###
# @deprecated
###
@jit(nopython=True)
def numba_bubble(index, new_val, data):
  for j in range(0, index):
    data[j] = data[j+1]
  data[index] = new_val
  return data

###
# @deprecated
###
@jit(nopython=True)
def numba_bubble_2d(index, new_val, data):
  for j in range(0, index):
    for k in range(0,3):
      data[j,k] = data[j+1,k]
  for k in range(0,3):
    data[index,k] = new_val[k]
  return data
