# Copyright 2015, 2016 Aaron Wise, Murat Can Cobanoglu
# 
# This file is part of DrugX.
# 
# DrugX is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# DrugX is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with DrugX.  If not, see <http://www.gnu.org/licenses/>.


###
#Loads RNA-Seq data from tcga into an hdf5 store.
#Each row of the store contains:
#gene_id (ENSEMBL FORMAT),
#normalized_count (RNA-Seq in log2 scale),
#cancer_type (coad, esca, lihc, etc.),
#barcode (TCGA identifier),
#tissue_type (normal or cancer),
#metastatic (boolean),
#individual (identifier for person)
#RNASeqV (1 or 2)
###

import sys
import re
import glob
import logging
import csv
import ensembl
import blaze
import numpy
import params_parser
import pandas as pd

###
#Parses the file which contains the mapping of file names to tcga barcodes.
###
def parse_metadata(f_name):
  file_to_barcode_map = {}
  with open(f_name, 'rb') as f:
    c = csv.reader(f, delimiter='\t')
    for line in c:
      if len(line)<2:
        logging.info('Line failed: '+str(line))
        continue
      file_to_barcode_map[line[0]] = line[1].split(',')[0] #sometimes, there are multiple vials of a sample, and we just choose one
  return file_to_barcode_map

###
#Helper function to map a tcga rnaseq file gene name to its ensembl id
###
def gene_id_parse(x, name_map=None, entrez_map=None):
  x = x.split('|')
  try:
    return name_map[x[0]]
  except KeyError:
    try:
      return entrez_map[x[1]]
    except:
      return None

###
#Helper function to map raw rnaseq reading to its processed form: log2(exp+1)
###
def count_parse(x):
  return numpy.log2(x+1)

###
#Main parse function that iterates over each of our files and adds it to our datastore
#cancer_types is a list of top-level tcga directories we wish to add to our database
#append is a boolean that determines whether we want to nuke to output file or not.
###
def parse_tcga(cancer_types, output_file='store.h5', append=False, path=''):
  output_file = path+output_file
  gene_list = None
  full_data = None
  (ens2gene, gene2ens, entrez2ens) = ensembl.build_ensembl_maps(path)
  if append:
    store = pd.HDFStore(output_file, mode='a')
  else:
    store = pd.HDFStore(output_file, mode='w')
  for cancer_type in cancer_types:
    logging.info('Starting parse of '+cancer_type.upper())
    try:
      file_to_barcode_map = parse_metadata(path+cancer_type+'/FILE_SAMPLE_MAP.txt')
      files = glob.glob(path+cancer_type+'/RNASeqV2/*/Level_3/*genes.normalized*')
      if len(files) == 0: #we need to go to RNASeqV1 mode
        files = glob.glob(path+cancer_type+'/RNASeq/*/Level_3/*gene.quantification*')
        RNASeqV1 = True
      else:
        RNASeqV1 = False
      logging.info("Number of files: "+ str(len(files)))
      count = 0
      for f in files:
        local_name = f.split('/')[-1]
        # Some files are absent from the FILE_SAMPLE_MAP
        if local_name in file_to_barcode_map:
          barcode = file_to_barcode_map[local_name]
        else:
          continue
        if RNASeqV1:
          ds = blaze.dshape("var * {gene: string, raw_counts: float64, median_length_normalized: float64, normalized_count: float64}")
        else:
          ds = blaze.dshape("var * {gene_id: string, normalized_count: float64}")
        b_csv = blaze.CSV(f, delimiter='\t', has_header=True)
        b = blaze.Data(b_csv, dshape=ds)
        b = blaze.odo(b, pd.DataFrame)
        if RNASeqV1: #we have to reformat things a little
          b.drop('raw_counts', axis=1, inplace=True)
          b.drop('median_length_normalized', axis=1, inplace=True)
          b.columns = ['gene_id','normalized_count']
        b['gene_id'] = b['gene_id'].apply(lambda x: gene_id_parse(x, name_map=gene2ens, entrez_map=entrez2ens))
        b['normalized_count'] = b['normalized_count'].apply(count_parse)
        b = b[pd.notnull(b['gene_id'])]
        b['cancer_type'] = cancer_type
        b['barcode'] = barcode
        if RNASeqV1:
          b['RNASeqV'] = 1
        else:
          b['RNASeqV'] = 2
        add_metadata_columns(b, barcode)
        #TODO: currently, we cannot add new categories and have to regenerate the store.  Is there a way around this?
        #TODO: there seems to be a weird bug where, if you flip the order of the categories, it doesn't work.  This needs to be investigated.
        b['tissue_type'] = b['tissue_type'].astype('category', categories=['cancer', 'normal'])
        b['cancer_type'] = b['cancer_type'].astype('category', categories=cancer_types)
        b['gene_id'] = b['gene_id'].astype('category')
        b['RNASeqV'] = b['RNASeqV'].astype('int8')
        #Build index and store in hdf
        try:
          nrows = store.get_storer('data').nrows
        except:
          nrows = 0
        b.index = pd.Series(range(len(b)))+nrows
        store.append('data', b, format='table', index=False, data_columns=True)
        count+=1
        logging.info('Count:'+str(count))
    except IOError as e:
      logging.error("I/O error({0}): {1}".format(e.errno, e.strerror))
  store.create_table_index('data', optlevel=9, kind='full')
  store.close()

###
#We parse the barcode to add metadata for normal vs disease
###
def add_metadata_columns(b, barcode):
  b['tissue_type'] = barcode_parse(barcode)
  b['metastatic'] = metastatic_parse(barcode)
  b['individual'] = individual_parse(barcode)

###
#Given a barcode, returns whether the tissue is normal or cancer
###
def barcode_parse(x):
  split_barcode = re.split('-', x)
  tissue = int(split_barcode[3][:2])
  if tissue>0 and tissue <10:
    return 'cancer'
  if tissue > 9 and tissue < 20:
    return 'normal'
  return None

###
#Given a barcode, returns whether the sample is from a metastasis
###
def metastatic_parse(x):
  split_barcode = re.split('-', x)
  tissue = int(split_barcode[3][:2])
  if tissue == 5 or tissue == 6:
    return True
  return False

###
#Given a barcode, returns a uniquely identifying id for each patient
###
def individual_parse(x):
  split_barcode = re.split('-', x)
  return split_barcode[1]+split_barcode[2]

if __name__ == '__main__':
  # Take raw data, turn into an hdf5 database
  param_in = sys.argv[1]
  params = params_parser.parse_yaml(param_in)
  logging.basicConfig(level=logging.INFO,
                      filename=params.identifier+'.log',
                      format='%(asctime)s %(name)-12s %(levelname)-8s %(message)s',
                      datefmt='%m-%d %H:%M:%S')
  logging.info('Program start: Main in tcga_parse.py')
  parse_tcga(params.cancer_list, output_file=params.store_file, append=params.store_append, path=params.datadir)
