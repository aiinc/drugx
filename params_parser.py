# Copyright 2015, 2016 Aaron Wise, Murat Can Cobanoglu
# 
# This file is part of DrugX.
# 
# DrugX is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# DrugX is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with DrugX.  If not, see <http://www.gnu.org/licenses/>.


import os
import yaml
import logging

class Params(object):

  def __init__(self, param_dict):
    self.matrix_mode = bool(param_dict["matrix_mode"]) if 'matrix_mode' in param_dict else False #Does matrix completion if True, o.w. does tensor completion
    self.passive_mode = bool(param_dict['passive_mode']) if 'passive_mode' in param_dict else False #Controls passive or active learning
    self.test_drug_start_index = None
    self.conv_thr = float(param_dict['conv_thr']) if 'conv_thr' in param_dict else 1e-5 #1e-5 is default
    self.log_level = getattr(logging,param_dict['log_level']) if 'log_level' in param_dict else logging.INFO
    self.store_file = param_dict['store_file'] if 'store_file' in param_dict else 'store.h5'
    self.store_append = param_dict['store_append'] if 'store_append' in param_dict else False
    self.combo_test = param_dict['combo_test'] if 'combo_test' in param_dict else False
    self.cancer_list = param_dict['cancer_list'] if 'cancer_list' in param_dict else ['hnsc', 'coad', 'prad', 'lihc']
    self.threshold_list= param_dict['threshold_list'] if 'threshold_list' in param_dict else [-15, -22, -15, -20]
    self.tensor_type = param_dict['tensor_type'] if 'tensor_type' in param_dict else 'B'
    self.rank = param_dict['rank'] if 'rank' in param_dict else 32
    self.test_type = param_dict['test_type'] if 'test_type' in param_dict else 'stitch3'
    self.train_percent = param_dict['train_percent'] if 'train_percent' in param_dict else 1.0
    self.datadir = param_dict['datadir'] if 'datadir' in param_dict else ''
    self.batch_size = param_dict['batch_size'] if 'batch_size' in param_dict else 5000
    self.batches = param_dict['batches'] if 'batches' in param_dict else 5
    self.stitch_threshold = param_dict['stitch_threshold'] if 'stitch_threshold' in param_dict else 400
    self.identifier = param_dict['identifier'] if 'identifier' in param_dict else \
                    '_'.join([self.test_type,
                              self.tensor_type,
                              'R'+str(self.rank),
                              'c'+str(self.stitch_threshold),
                              '{0:d}x{1:d}'.format(self.batches,self.batch_size)])
    # The percentage of scored drug-disease interactions that we take to be real
    self.true_threshold = param_dict['true_threshold'] if 'true_threshold' in param_dict else 0.1
    # The percentage to leave out in cross-validation runs
    self.leave_out = param_dict['leave_out'] if 'leave_out' in param_dict else 0.1
    # The value in the tensor used when we infer activity across drugs
    self.cross_drug_strength = param_dict['cross_drug_strength'] if 'cross_drug_strength' in param_dict else 0.25
    self.stitch_regen = param_dict['stitch_regen'] if 'stitch_regen' in param_dict else False
    self.cp_init = param_dict['cp_init'] if 'cp_init' in param_dict else 'random' #'nvecs' or 'random'
    self.init_seed = param_dict['init_seed'] if 'init_seed' in param_dict else 0 #only meaningful if cp_init == 'random'
    self.cv_seed = param_dict['cv_seed'] if 'cv_seed' in param_dict else 0 #only meaningful if test_type involves cross-val
    self.tp_seed = param_dict['tp_seed'] if 'tp_seed' in param_dict else 0 #only meaningful if train_percent < 1
    self.plot = param_dict['plot'] if 'plot' in param_dict else False #plot hits/preds and improv_factor/batches plots ?
    if self.datadir != '' and self.datadir[-1]!=os.sep:
      self.datadir+=os.sep

def parse_yaml(f_name):
  with open(f_name, 'rb') as f:
    return Params(yaml.load(f))
