# Copyright 2015, 2016 Aaron Wise, Murat Can Cobanoglu
# 
# This file is part of DrugX.
# 
# DrugX is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# DrugX is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with DrugX.  If not, see <http://www.gnu.org/licenses/>.


###
# TODO: investigate building a unified graph combining
# Object that stores the graph-based information relating our drugs, diseases and genes.
# Tries to build drug-disease interactions based on mutual target information
###

import stitch_parse
from tcga_analyze import AnalysisTuple
import tcga_analyze
import numpy
import logging
import networkx as nx
import cPickle as pickle
import params_parser
#import joblib

###
# TODO: pass through regenerate
# Attributes:
#   g_drug
#   g_disease
#   drugs
#   diseases
#   genes
#   disease_drug_links
#   path (to all our data files)
#   stitch_version
#   regenerate (if false, we preferentially use pickled results.  Otherwise we compute everything from scratch.)
###
class DiDrGraph(object):

  def __init__(self, params, stitch_version='4.0', regenerate='false'):
    self.params = params
    self.stitch_version = stitch_version
    self.find_disease_drug_links()
    self.regenerate = regenerate

  ###
  # Given a disease and drug, we calculate the sum of the weights of the genes they hold in common.
  # TODO: we need to prevent stuff like ATP from giving a good score.  Currently we divide by ||di U dr ||^ 0.5
  # but that doesn't seem to be enough to prevent promiscuous 'drugs'.  We also use a blacklist / mol weight filter
  # but again, there seem to be more to do here.
  # TODO: right now we return all common edges, but we may want to threshold it by score.
  # TODO: we currently ignore the direction that a gene is perturbed
  ###
  def disease_drug_score(self, disease, drug, g_disease, g_drug):
    di_edges = g_disease.edges(disease)
    di_genes = set([edge[1] for edge in di_edges])
    dr_edges = g_drug.edges(drug)
    dr_genes = set([edge[1] for edge in dr_edges])
    common_edges = di_genes.intersection(dr_genes)
    score = 0.0
    for edge in common_edges:
      score += g_disease[disease][edge]['weight'] * g_drug[drug][edge]['weight'] * self.determine_sign(g_disease[disease][edge]['sign'], g_drug[drug][edge]['sign'])
      #TODO: what do we want to do about negative edges on didr interactions that are highly weighted as a whole?  So far we include
      #them as 1's in our tensor -- is that a good idea?  probably not.
    return (score/numpy.sqrt(len(di_genes.union(dr_genes))), common_edges)

  ###
  # If one of the signs is 0 (unknown) or the signs are different (compensatory)
  # we return 1.  Else, if the signs are the same, we return -1
  ###
  def determine_sign(self, di_sign, dr_sign):
    if di_sign == 0 or dr_sign == 0:
      return 1
    else:
      return -di_sign * dr_sign

  ###
  # Integrates our drug-target and disease-target graphs, returning a list of tuples.  Each tuple contains:
  # disease (string)
  # drug (string) in STITCH CID format
  # genes (set of strings) in ENSG format
  # score (float).  Higher scores are better
  ###
  def find_disease_drug_links(self):
    score_tuples = []
    (g_disease, disease_genes, diseases) = tcga_analyze.build_disease_graph(self.params)
    logging.info("Done building disease graph")
    (g_drug, stitch_genes, stitch_chemicals) = stitch_parse.build_stitch_graph(
            path=self.params.datadir,
            version=self.stitch_version,
            regenerate=self.params.stitch_regen,
            cutoff=self.params.stitch_threshold)
    logging.info(nx.info(g_disease))
    logging.info(nx.info(g_drug))
    #TODO: to parallelize this efficiently, we'd have to set up a memory map.  I don't think it's worth the effort
    #score_tuples = joblib.Parallel(n_jobs=8)(joblib.delayed(disease_drug_score)(disease, drug, g_disease, g_drug)
    #        for disease in diseases for drug in stitch_chemicals)
    for disease in diseases:
      for drug in stitch_chemicals:
        (score, genes) = self.disease_drug_score(disease, drug, g_disease, g_drug)
        if score > 0:
          score_tuples.append((disease, drug, genes, score))
    sorted_tuples = sorted(score_tuples, key=lambda x: -x[3])
    self.g_drug = g_drug
    self.g_disease = g_disease
    self.diseases = diseases
    self.drugs = stitch_chemicals
    self.genes = list(set(disease_genes).union(set(stitch_genes)))
    self.disease_drug_links = numpy.array(sorted_tuples)

def create_didr(params, stitch_version='4.0', regenerate=False):
  filename = params.datadir+'didr_'+stitch_version+\
		'_c'+str(params.stitch_threshold)+'.pickle'
  if not regenerate:
    try:
      with open(filename, 'rb') as f:
        didr = pickle.load(f)
    except IOError:
        regenerate = True
  if regenerate:
    didr = DiDrGraph(params=params, stitch_version=stitch_version, regenerate=regenerate)
    with open(filename, 'wb') as f:
      pickle.dump(didr, f)
  return didr

if __name__ == '__main__':
  param_in = sys.argv[1]
  params = params.parse_yaml(param_in)
  logging.basicConfig(level=logging.INFO,
                      filename=params.identifier+'.log',
                      format='%(asctime)s %(name)-12s %(levelname)-8s %(message)s',
                      datefmt='%m-%d %H:%M')
  logging.info('Program start: Main in drug_disease.py')
  didr = create_didr(params)
  logging.info(nx.info(didr.g_disease))
  logging.info(nx.info(didr.g_drug))
