# Copyright 2015, 2016 Aaron Wise, Murat Can Cobanoglu
# 
# This file is part of DrugX.
# 
# DrugX is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# DrugX is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with DrugX.  If not, see <http://www.gnu.org/licenses/>.


###
# Builds a networkx graph from the stitch drug-gene links.
###

import os
import csv
import logging
import pandas as pd
import networkx as nx
import params_parser
import ensembl
import cPickle as pickle

###
# Returns a networkx graph containing drug-gene links in STITCH
# Note that we remove links for which we can't map the ENSP to a ENSG.  We may be able to do a better job there.
# By default we filter predictions to the medium (0.400) level.
###
def build_stitch_graph(path="", version='4.0', regenerate=False, cutoff=400):
  #TODO: dosomething better with this.  It contains ATP and other stuff that we just want to exclude.
  chem_blacklist = set(['CID100000197', 'CID000006022', 'CID000006830','CID000008977', 'CID100000730'])
  filename = path+'stitch_graph'+version+'_c'+str(cutoff)+'.pickle'
  if os.path.isfile(filename) and not(regenerate):
    try:
      with open(filename, 'rb') as f:
        (g, all_genes, all_chemicals) = pickle.load(f)
        return (g, all_genes, all_chemicals)
    except IOError:
      regenerate = True
  g = nx.Graph()
  missing = 0
  chem = valid_chemical_set(path=path)
  all_genes = set()
  all_chemicals = set()
  ensp2ensg = ensembl.build_ensp_ensg(path=path)
  activity_map = build_activity_map(path=path)
  datafile = path+'human_protein_chemical.links.v'+version+'.tsv'
  try:
    with open(datafile, 'rb') as f:
      c = csv.reader(f, delimiter='\t')
      for line in c:
        chemid = line[0]
        if chemid not in chem or chemid in chem_blacklist:
          continue
        protein = line[1].split('.')[1]
        try:
          gene = ensp2ensg[protein]
        except KeyError:
          missing+=1
          continue
        weight = float(line[2])
        if weight < cutoff:
          continue
        all_genes.add(gene)
        all_chemicals.add(chemid)
        if (chemid, gene) in activity_map:
          sign = activity_map[(chemid, gene)]
        else:
          sign = 0
        g.add_edge(chemid, gene, weight=weight, sign=sign)
  except IOError:
    logging.error('{0:s} not found, STITCH cannot be built. Fatal Error.'.format(datafile))
    exit()
  logging.debug("STITCH links not found in ENSG: "+ str(missing))
  logging.debug("all chemicals: "+str(len(all_chemicals)))
  (g, all_chemicals) = dedup_graph(g, all_chemicals)
  logging.debug("all chemicals: "+str(len(all_chemicals)))
  with open(filename, 'wb') as f:
    pickle.dump((g, all_genes, all_chemicals), f)
  return (g, all_genes, all_chemicals)

def dedup_graph(g, all_chemicals):
  dup_count = 0
  new_all_chemicals = set()
  for chemical in all_chemicals:
    if chemical[:4]=='CID1':
      c1 = 'CID1' + chemical[4:]
      c0 = 'CID0' + chemical[4:]
      edges_1 = g.edges(c1)
      edges_0 = g.edges(c0)
      genes_1 = set([edge[1] for edge in edges_1])
      genes_0 = set([edge[1] for edge in edges_0])
      if genes_1 == genes_0:
        g.remove_node(chemical)
        dup_count+=1
      else:
        new_all_chemicals.add(chemical)
    else:
      new_all_chemicals.add(chemical)
  logging.debug("Number of duplicate chemicals removed: " + str(dup_count))
  return (g, new_all_chemicals)

###
# Returns a set of all chemicals that meet our criteria:
#   Mol weight > 150
###
def valid_chemical_set(path=""):
  try:
    with open(path+'chemical_set_001.pickle', 'rb') as f:
      chem = pickle.load(f)
  except IOError:
    chem = pd.read_csv(path+'chemicals.v4.0.tsv', delimiter='\t')
    chem = chem.query('molecular_weight > 150')
    chem = set(chem['chemical'].tolist())
    with open(path+'chemical_set_001.pickle', 'wb') as f:
      pickle.dump(chem, f)
  return chem

###
# Currently, all we look at is the 'activity' column, and whether it is activation or inhibition.
# For everything else, we will give it a 'no clue' flag in the graph
###
def build_activity_map(path=''):
  activity_map = {}
  ensp2ensg = ensembl.build_ensp_ensg(path)
  with open(path+'human_actions.v4.0.tsv', 'rb') as f:
    c = csv.reader(f, delimiter='\t')
    c.next()
    for line in c:
      if line[3] == 'activation' or line[3] == 'inhibition':
        if line[1][:3] == 'CID':
          chem = line[1]
          protein = line[0].split('.')[1]
        else:
          chem = line[0]
          protein = line[1].split('.')[1]
        try:
          gene = ensp2ensg[protein]
        except KeyError:
          continue
        if line[3] == 'activation':
          activity = 1
        else:
          activity = -1
        activity_map[(chem, gene)] = activity
  return activity_map

if __name__ == '__main__':
  (g, all_genes, all_chemicals) = build_stitch_graph(version='4.0')
  print nx.info(g)
  (g, all_genes, all_chemicals) = build_stitch_graph(version='3.1')
  print nx.info(g)
