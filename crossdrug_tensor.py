# Copyright 2015, 2016 Aaron Wise, Murat Can Cobanoglu
# 
# This file is part of DrugX.
# 
# DrugX is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# DrugX is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with DrugX.  If not, see <http://www.gnu.org/licenses/>.


import bio_tensor
import drug_similarity
import logging
import numpy
from collections import defaultdict

###
# Subclass of BTensor that also performs inference across drugs by putting low weight
# entries for interactions when similar drugs perform similarly. Testing restricted to
# de novo drugs.
###
class CDTensor(bio_tensor.BTensor):
  def __init__(self, entries, drugs, diseases, genes, params, drsimid='v4'):
    self.drsim = drug_similarity.create_drsim(path=params.datadir, drugs=drugs, identifier=drsimid, regenerate=False)
    self.hit_map = defaultdict(set)
    n_drugs = len(drugs)
    # Store the train/test split index
    self.test_start = params.test_drug_start_index
    # Resume initialization
    super(CDTensor, self).__init__(entries, drugs, diseases, genes, params)

  ###
  # We override add_data to add additional entries for cross-drug inference.
  # Currently, we simply add hits (di,dr,g) with strength = max_(n in neighbors)  (0.25 * similarity_{dr,n}^2 * I[di,n,g])
  ###
  def add_data(self, entries):
    for (disease, drug, genes, score) in entries:
      self.hit_map[(disease, drug)] = self.hit_map[(disease, drug)].union(set(genes))
      for gene in genes:
        self.entries[(self.disease_to_index[disease], self.drugs_to_index[drug], self.genes_to_index[gene])] = 1
        self.known_hits.add((self.disease_to_index[disease], self.drugs_to_index[drug], self.genes_to_index[gene]))
    logging.info('Adding inferred interactions')
    logging.info('Entries before inference: ' + str(len(self.entries)))
    for disease in self.diseases:
      for drug in self.drugs:
        neighbors = self.drsim.similarities[drug]
        for (neighbor, score) in neighbors[2::-1]:
          #we are limited by our tensor decomposition speed.  To make more inferences, we will need a faster implementation
          if score < 0.95:
            continue
          infer_genes = self.hit_map[(disease, neighbor)].difference(self.hit_map[(disease, drug)])
          for gene in infer_genes:
            self.entries[(self.disease_to_index[disease], self.drugs_to_index[drug], self.genes_to_index[gene])] = self.params.cross_drug_strength * score * score
    logging.info('Entries after inference: ' + str(len(self.entries)))
    logging.info('Done adding inferred interactions')
    self.build_sptensor()

  ###
  # We override to restrict predictions to de novo only
  # chunksize: how many drugs we calculate per sub-function call.  Proportional to memory usage
  ###
  def chunked_make_predictions(self, indices_to_rank=[], chunksize=50000, n=100):
    index_scores = self.subset_test_set(indices_to_rank)
    #worst_index_score = numpy.min(index_scores)
    index_rank = numpy.zeros(len(index_scores))
    hq = [(0.0, (0,0,0)) for i in range(n)]
    current_worst = 0.0
    chunkstarts = range(self.test_start, len(self.drugs), chunksize)
    num_values = chunksize*len(self.genes)
    for i in range(0, len(self.diseases)):
      for dr_min in chunkstarts:
        dr_max = numpy.min([len(self.drugs), dr_min+chunksize]) #either we stop with a full chunk, or at the end of the list
        # set up variables for jit function
        scores = numpy.zeros(num_values)
        indices = numpy.zeros((num_values, 3), dtype=int)
        logging.info("Beginning chunk")
        (num_assigned, indices, scores) = bio_tensor.jit_predict_v2(scores, indices, self.P.lmbda, self.P.U[0], self.P.U[1], self.P.U[2], i, i+1,
                                           dr_min, dr_max, len(self.genes))
        scores = scores[:num_assigned]
        indices = indices[:num_assigned]
        logging.info("Sorting chunk results")
        order = numpy.argsort(scores)
        scores = scores[order]
        indices = indices[order]
        logging.info("Evaluating test set")
        index_rank = bio_tensor.evaluate_test_set(index_rank, index_scores, scores)
        logging.info("maintaining top-n")
        (current_worst, hq) = self.update_heapq(current_worst, hq, scores, indices)
        logging.info("done with chunk dis:"+str(i)+", drug:"+str(dr_min)+" to "+str(dr_max))
        logging.debug(hq)
    self.current_predictions = sorted(hq, key=lambda x: x[0])
    #adjust index ranks for  training set
    train_scores = numpy.array([self.single_predict(*coord) for coord in self.known_hits])
    index_rank = bio_tensor.adjust_train_set(index_rank, index_scores, train_scores)
    return index_rank
    logging.debug(hq[:50])

