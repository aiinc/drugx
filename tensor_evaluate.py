# Copyright 2015, 2016 Aaron Wise, Murat Can Cobanoglu
# 
# This file is part of DrugX.
# 
# DrugX is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# DrugX is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with DrugX.  If not, see <http://www.gnu.org/licenses/>.


###
# Builds subset tensors and evaluates our predictive performance.
###

import sys
import copy
import cPickle as pickle
import logging
from tensor_runner import TensorRunner
from drug_disease import create_didr
from sklearn import cross_validation
import ensembl
import numpy
from tcga_analyze import AnalysisTuple
import params_parser


class TensorEvaluator(object):

  def __init__(self, params):
    self.params = params

  ###
  # Cross validation using the stitch4 data as both train and test
  # Optional parameters:
  #   hits: if 0 < hits < 1, the fraction of
  #   leave_out: fraction of positive edges to leave out of the tensor
  ###
  def s4_cv_tensor(self, leave_out=0.1, hits=0.1):
    logging.info('Running stitch 4 cross validation')
    didr = create_didr(params=self.params)
    positive_set = didr.disease_drug_links
    if hits > 1:
      positive_set = positive_set[:hits]
    else:
      index = int(hits * float(len(positive_set)))
      positive_set = positive_set[:index]
    rs = cross_validation.ShuffleSplit(len(positive_set), n_iter=1, test_size=leave_out)
    for (train_indices, test_indices) in rs:
      train_set = positive_set[train_indices]
      test_set = positive_set[test_indices]
      return (train_set, test_set, didr.drugs, didr.diseases, didr.genes)

  ###
  # Cross validation training on stitch 3.1 and testing on stitch 4
  ###
  def s3_cv_tensor(self, hits=0.1):
    logging.info('Training on stitch 3, testing on stitch 4')
    didr3 = create_didr(params=self.params,
                        stitch_version='3.1')
    didr4 = create_didr(params=self.params,
                        stitch_version='4.0')
    positive_set = didr4.disease_drug_links
    if hits > 1:
      positive_set = positive_set[:hits]
    else:
      index = int(hits * float(len(positive_set)))
      positive_set = positive_set[:index]
    train_set = []
    test_set = []
    for (disease, drug, genes, score) in positive_set:
      if not drug in didr3.g_drug:
        test_set.append((disease, drug, genes, score))
      else:
        yes_genes = []
        no_genes = []
        genes_of_drug = didr3.g_drug[drug]
        for gene in genes:
          if gene in genes_of_drug:
            yes_genes.append(gene)
          else:
            no_genes.append(gene)
        if len(no_genes) > 0:
          test_set.append(((disease, drug, set(no_genes), score)))
        if len(yes_genes) > 0:
          train_set.append(((disease, drug, set(yes_genes), score)))
    test_set = numpy.array(test_set)
    train_set = numpy.array(train_set)
    return (train_set, test_set, didr4.drugs, didr4.diseases, didr4.genes)

  ###
  # Cross validation using the stitch4 data, but leaving out drugs randomly
  # This test setup works with crossdrug tensor
  # Optional parameters:
  #   hits: if 0 < hits < 1, the fraction of
  #   leave_out: fraction of positive edges to leave out of the tensor
  ###
  def drug_leaveout_cv_tensor(self, leave_out=0.1, hits=0.1):
    logging.info('Running random drug leaveout cross validation on stitch_v4')
    didr = create_didr(params=self.params,
                       stitch_version='4.0')
    positive_set = didr.disease_drug_links
    if hits > 1:
      positive_set = positive_set[:hits]
    else:
      index = int(hits * float(len(positive_set)))
      positive_set = positive_set[:index]
    drugs = list(didr.drugs)
    numpy.random.seed(self.params.cv_seed)
    numpy.random.shuffle(drugs)
    split_index = int(leave_out*float(len(drugs)))
    test_drugs = set(drugs[:split_index])
    train_set = []
    test_set = []
    for (disease, drug, genes, score) in positive_set:
        if drug in test_drugs:
            test_set.append((disease, drug, genes, score))
        else:
            train_set.append((disease, drug, genes, score))
    test_set = numpy.array(test_set)
    train_set = numpy.array(train_set)
    drugs = sorted([(drug,drug in test_drugs) for drug in didr.drugs],\
                     key = lambda x:x[1])
    # Remove the train/test label
    drugs = [drug[0] for drug in drugs]
    self.params.test_drug_start_index = len(drugs)-len(test_drugs)
    return (train_set, test_set, drugs, didr.diseases, didr.genes)

  ###
  # We train on all drugs that existed in stitch3 (even including their stitch4 entries) and test on
  # the drugs that only exist in stitch4
  # This test setup works with crossdrug tensor
  ###
  def drug_leaveout_tensor(self, hits=0.1):
    logging.info('Running drug leaveout train/test setup')
    didr3 = create_didr(params=self.params,
                        stitch_version='3.1')
    didr4 = create_didr(params=self.params,
                        stitch_version='4.0')
    positive_set = didr4.disease_drug_links
    if hits > 1:
      positive_set = positive_set[:hits]
    else:
      index = int(hits * float(len(positive_set)))
      positive_set = positive_set[:index]
    train_set = []
    test_set = []
    test_drugs = set()
    for (disease, drug, genes, score) in positive_set:
      if not drug in didr3.g_drug:
        test_set.append((disease, drug, genes, score))
        test_drugs.add(drug)
      else:
        train_set.append((disease, drug, genes, score))
    test_set = numpy.array(test_set)
    train_set = numpy.array(train_set)
    drugs = sorted([(drug,drug in test_drugs) for drug in didr4.drugs],\
                     key = lambda x:x[1])
    # Remove the train/test label
    drugs = [drug[0] for drug in drugs]
    self.params.test_drug_start_index = len(drugs)-len(test_drugs)
    return (train_set, test_set, drugs, didr4.diseases, didr4.genes)

  ###
  # Training on stitch3 (from 2012), and test on all FDA approved cancer drugs after 2012.
  # This gives exactly 13 disease-drug interactions to find.  Talk about a needle in a haystack.
  ###
  def cancer_drug_tensor(self, hits=0.1):
    logging.info('Running cancer drug train/test setup')
    cancer_cids = ['CID005330286', 'CID006918638', 'CID057379345', 'CID011625818', 'CID023725625', 'CID024821094', 'CID044462760', 'CID011707110', 'CID000134780', 'CID010184653', 'CID006450551', 'CID005328940', 'CID011556711', 'CID000285033', 'CID024776445', 'CID025102847', 'CID011167602', 'CID15951529', 'CID024826799']
    didr3 = create_didr(params=self.params,
                        stitch_version='3.1')
    didr4 = create_didr(params=self.params,
                        stitch_version='4.0')
    positive_set = didr4.disease_drug_links
    if hits > 1:
      positive_set = positive_set[:hits]
    else:
      index = int(hits * float(len(positive_set)))
      positive_set = positive_set[:index]
    train_set = []
    test_set = []
    for (disease, drug, genes, score) in positive_set:
      if drug in cancer_cids:
        test_set.append(((disease, drug, genes, score)))
      elif drug in didr3.g_drug:
        yes_genes = []
        for gene in genes:
          if gene in didr3.g_drug[drug]:
            yes_genes.append(gene)
        if len(yes_genes) > 0:
          train_set.append(((disease, drug, set(yes_genes), score)))
    test_set = numpy.array(test_set)
    train_set = numpy.array(train_set)
    return (train_set, test_set, didr4.drugs, didr4.diseases, didr4.genes)

  def get_data(self):
    if self.params.test_type == 'stitch4':
      (train_set, test_set, drugs, diseases, genes) = self.s4_cv_tensor(hits=self.params.true_threshold, leave_out=self.params.leave_out)
    elif self.params.test_type == 'stitch4_notest':
      (train_set, test_set, drugs, diseases, genes) = self.s4_cv_tensor(hits=self.params.true_threshold, leave_out=0.0)
    elif self.params.test_type == 'stitch3':
      (train_set, test_set, drugs, diseases, genes) = self.s3_cv_tensor(hits=self.params.true_threshold)
    elif self.params.test_type == 'drug_leaveout_cv':
      (train_set, test_set, drugs, diseases, genes) = self.drug_leaveout_cv_tensor(hits=self.params.true_threshold, leave_out=self.params.leave_out)
    elif self.params.test_type == 'drug_leaveout':
      (train_set, test_set, drugs, diseases, genes) = self.drug_leaveout_tensor(hits=self.params.true_threshold)
    elif self.params.test_type == 'cancer_leaveout':
      (train_set, test_set, drugs, diseases, genes) = self.cancer_drug_tensor(hits=self.params.true_threshold)
    return (train_set, test_set, drugs, diseases, genes)

  def run_test(self):
    (train_set, test_set, drugs, diseases, genes) = self.get_data() 
    if self.params.matrix_mode:
      for i,disease in enumerate(self.params.cancer_list):
        logging.debug("disease: {:g} {:}".format(i,disease))
        dis_train_set = []; dis_test_set = []
        for old_set,  new_set in [(train_set, dis_train_set), (test_set, dis_test_set)]:
          for (disease2, drug2, genes2, score2) in old_set:
            if disease == disease2:
              new_set.append(((disease2,  drug2, genes2, score2)))
          new_set = numpy.array(new_set)
        dis_params = copy.deepcopy(self.params)
        dis_params.identifier += '.'+disease
        dis_params.batch_size /= len(self.params.cancer_list)
        TR = TensorRunner(dis_params)
        TR.run_trial(dis_train_set, dis_test_set, drugs, [disease], genes, dis_params.batches, dis_params.batch_size)
    else:
      TR = TensorRunner(self.params)
      TR.run_trial(train_set, test_set, drugs, diseases, genes, self.params.batches, self.params.batch_size)


if __name__ == '__main__':
  param_in = sys.argv[1]
  params = params_parser.parse_yaml(param_in)
  logging.basicConfig(level=params.log_level,
                      filename=params.identifier+'.log',
                      format='%(asctime)s %(name)-12s %(levelname)-8s %(message)s',
                      datefmt='%m-%d %H:%M:%S')
  logging.info('Program start: Main in tensor_evaluate.py')
  TE = TensorEvaluator(params)
  TE.run_test()
