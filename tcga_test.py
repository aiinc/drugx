# Copyright 2015, 2016 Aaron Wise, Murat Can Cobanoglu
# 
# This file is part of DrugX.
# 
# DrugX is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# DrugX is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with DrugX.  If not, see <http://www.gnu.org/licenses/>.


import unittest
import pandas as pd

class TCGATest(unittest.TestCase):
     
  def testTCGA_db(self):
    df = pd.read_hdf('store.h5', 'data')
    df['barcode'] = df['barcode'].apply(lambda x: x[:15])
    df_1 = df[df['barcode'] == 'TCGA-AA-3529-01']
    for row in df_1:
      self.assertEqual(len(df_1), len(df_1[df_1['cancer_type'] == 'coad']))
      self.assertEqual(len(df_1), len(df_1[df_1['tissue_type'] == 'cancer']))
    df_1 = df[df['barcode'] == 'TCGA-A6-2680-11']
    for row in df_1:
      self.assertEqual(len(df_1), len(df_1[df_1['cancer_type'] == 'coad']))
      self.assertEqual(len(df_1), len(df_1[df_1['tissue_type'] == 'normal']))
    df_1 = df[df['barcode'] == 'TCGA-CV-6954-01']
    for row in df_1:
      self.assertEqual(len(df_1), len(df_1[df_1['cancer_type'] == 'hnsc']))
      self.assertEqual(len(df_1), len(df_1[df_1['tissue_type'] == 'cancer']))
    self.assertTrue(abs(df_1[df_1['gene_id'] == 'ENSG00000143322']['normalized_count'].values[0] - 9.90918) < 0.01)
    self.assertTrue(abs(df_1[df_1['gene_id'] == 'ENSG00000106541']['normalized_count'].values[0] - 9.063) < 0.01)

if __name__ == '__main__':
  unittest.main()