# Copyright 2015, 2016 Aaron Wise, Murat Can Cobanoglu
# 
# This file is part of DrugX.
# 
# DrugX is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# DrugX is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with DrugX.  If not, see <http://www.gnu.org/licenses/>.


###
# Runs a single instance of iterative tensor learning.
###

import csv
import logging
import cPickle as pickle
import math
import numpy
import matplotlib
matplotlib.use('Agg') # Needed to run on the server
from matplotlib import pyplot as plot
from tcga_analyze import AnalysisTuple
from itertools import combinations
from collections import defaultdict, namedtuple
from numpy.random import shuffle
from drug_disease import create_didr
from bio_tensor import BTensor
from crossdrug_tensor import CDTensor
from tensor_runner_record import TensorRunnerRecord
import ensembl

ChemoTuple = namedtuple('ChemoTuple', ['cid_i', 'cid_j', 'score'])



class TensorRunner(object):

  ###
  #baseline_discovery_rate: rate that a test example would be chosen at random.
  #old_predictions: we keep track of the predictions from the previous iteration, to see how much
  #  retesting we are doing.  Each item in the set is an index_entry
  #all_old_predictions: union of old_predictions at every iteration.  Contains index_entries
  ###
  def __init__(self, params):
    self.n_predictions_made = [0.0]
    self.n_predictions_correct = [0.0]
    self.n_predictions_stitch = [0.0]
    self.learning_factor = [0.0]
    self.cumulative_learning_factor = [0.0]
    self.baseline_discovery_rate = 0
    self.old_predictions = set()
    self.all_old_predictions = set()
    self.didr = create_didr(params)
    self.tensor_type = params.tensor_type
    self.params = params

  ###
  #Given a train and test set, runs our tensor, adds correct results, and re-runs $batches number of times.
  ###
  def run_trial(self, train_set, test_set, drugs, diseases, genes, batches=5, batch_size=5000):
    if self.params.train_percent < 1:
      train_threshold = int(self.params.train_percent * len(train_set))
      numpy.random.seed(self.params.tp_seed)
      shuffle(train_set)
      train_set = train_set[:train_threshold]
    logging.info("train set size (Di-Dr): "+str(len(train_set)))
    logging.info("test set size (Di-Dr): "+str(len(test_set)))
    logging.debug("sample training set entry: "+str(train_set[1]))
    if self.tensor_type == 'CD':
      logging.info("Using CD tensor")
      t = CDTensor(train_set, drugs, diseases, genes, self.params)
    else:
      logging.info("Using B tensor")
      t = BTensor(train_set, drugs, diseases, genes, self.params)
    formatted_test_set = t.as_index_entries_genes(test_set)
    formatted_test_set = t.as_name_entries(formatted_test_set)
    logging.info("train set size: "+str(len(t.as_index_entries_genes(train_set))))
    logging.info("test set size: "+str(len(formatted_test_set)))
    if self.params.test_drug_start_index is None:
      self.baseline_discovery_rate = float(len(formatted_test_set)) / \
        float( len(drugs)*len(diseases)*len(genes) - len(formatted_test_set) )
    else:
      num_dr = len(drugs); num_di = len(diseases); num_g = len(genes);
      num_test_drugs = (num_dr-self.params.test_drug_start_index)
      self.baseline_discovery_rate = float(len(formatted_test_set)) / \
        float(num_test_drugs*num_di*num_g)
    if self.params.combo_test:
      self.evaluate_combos(t)
    for batch in range(0, batches):
      if self.params.combo_test:
        self.evaluate_combos(t)
      test_ranks = t.chunked_make_predictions(indices_to_rank=t.as_index_entries_genes(test_set), n=batch_size)
      self.analyze_test_ranks(test_ranks)
      predictions_correct = self.evaluate_predictions(t, formatted_test_set)
      self.old_predictions = set([x[1] for x in t.current_predictions])
      self.all_old_predictions = self.all_old_predictions.union(self.old_predictions)
      #run update with correct predictions, incorrect predictions.
      t.update(predictions_correct, self.old_predictions-t.as_index_entries_gene(predictions_correct))
    print self.n_predictions_correct
    print self.n_predictions_made
    TensorRunnerRecord.save(self)
    if self.params.plot:
    # Plot absolute hit counts:
      baseline_hits = numpy.array([n*self.baseline_discovery_rate for n in self.n_predictions_made])
      plot.plot(self.n_predictions_made, self.n_predictions_correct)
      plot.plot(self.n_predictions_made, baseline_hits)
      plot.xlabel('No of Predictions')
      plot.ylabel('No of Hits')
      plot.title('Algorithmic Ideation (AI) vs Baseline Perfomance')
      plot.legend(['AI','Baseline'])
      plot.savefig(self.params.identifier+'_prediction_curve.png')
      plot.clf()
      # Plot multiplicate improvement over baseline:
      improv = numpy.array(self.n_predictions_correct)/baseline_hits
      plot.bar(range(len(self.n_predictions_made)), improv, align='center')
      plot.xlabel('Batch')
      plot.ylabel('Fold-improvement')
      plot.title('Algorithmic Ideation Learning Factor')
      plot.savefig(self.params.identifier+'_fold_improv.png')
      plot.clf()


  ###
  #Determines how well we do in identifying our test set compared to random
  #TODO: get rid of magic number 5 (which represents the non-randomly selected top-5)
  ###
  def analyze_test_ranks(self, test_ranks):
    if len(test_ranks > 0):
      logging.info("Mean test rank: "+str(numpy.mean(test_ranks[5:])))
      logging.info("Median test rank: "+str(numpy.median(test_ranks[5:])))
      logging.info("Best test rank: "+str(numpy.min(test_ranks)))
      logging.info("Worst test rank: "+str(numpy.max(test_ranks)))
      logging.info("Sum of test ranks: "+str(numpy.sum(test_ranks[5:])))


  ###
  # Determines which predictions are in the test set, and stores relevant statistics
  ###
  def evaluate_predictions(self, t, formatted_test_set):
    if len(formatted_test_set) == 0:
      return []
    (ens2gene, gene2ens, entrez2ens) = ensembl.build_ensembl_maps(self.params.datadir)
    predictions = set([t.as_name_entry(i,j,k) for (score, (i,j,k)) in t.current_predictions])
    predictions_correct = set(formatted_test_set).intersection(set(predictions))
    self.n_predictions_made.append(self.n_predictions_made[-1] + len(predictions))
    self.n_predictions_correct.append(self.n_predictions_correct[-1] + len(predictions_correct))
    self.learning_factor.append((float(len(predictions_correct)) / float(len(predictions))) / self.baseline_discovery_rate)
    self.cumulative_learning_factor.append((float(self.n_predictions_correct[-1]) / float(self.n_predictions_made[-1])) / self.baseline_discovery_rate)
    in_stitch = 0
    self.get_cancer_distribution([(i,j,k) for (score, (i,j,k)) in t.current_predictions])
    self.get_cancer_distribution(predictions_correct)
    other_cancer_hits = self.in_other_cancer(t, t.current_predictions)
    logging.info("Predictions inferred across cancers: " + str(other_cancer_hits))
    #correct_other_cancer_hits = self.in_other_cancer(t, t.current_predictions) #TODO: need to format entries
    for p in t.current_predictions:
      p = t.as_name_entry(*p[1])
      if self.didr.g_drug.has_edge(p[1], p[2]):
        in_stitch += 1
    self.n_predictions_stitch.append(self.n_predictions_stitch[-1] + in_stitch)
    self.number_predictions_the_same(t.current_predictions)
    logging.info("predictions made: " + str(self.n_predictions_made))
    logging.info("predictions in stitch: " + str(self.n_predictions_stitch))
    logging.info("predictions correct: " + str(self.n_predictions_correct))
    logging.info("learning factor: " + str(self.learning_factor))
    logging.info("cumulative learning factor: " + str(self.cumulative_learning_factor))
    readable_results = []
    for (score, (disease, drug, gene)) in t.current_predictions[:50]:
      (disease, drug, gene) = t.as_name_entry(disease, drug, gene)
      if gene in ens2gene:
        readable_results.append((disease, drug, ens2gene[gene]))
    logging.info("TOP RESULTS:")
    logging.info(readable_results)
    logging.debug("Example test set items:")
    logging.debug(list(formatted_test_set)[:50])
    return predictions_correct

  ###
  # For each of our predictions, determines if it's in the training set for another tumor.
  # I.e., this determines how many predictions were only inferred across cancer (for same drug and gene)
  ###
  def in_other_cancer(self, t, current_predictions):
    other_cancer_hits = 0
    for (score, (i,j,k)) in current_predictions:
      for other_i in range(len(t.diseases)):
        if (other_i, j, k) in t.entries:
          if t.entries[(other_i, j, k)] == 1:
            other_cancer_hits +=1
            break #can only hit once per prediction
    return other_cancer_hits

  ###
  # Finds how many of our predictions for this iteration are associated with each cancer type
  ###
  def get_cancer_distribution(self, current_predictions):
    distribution = defaultdict(int)
    for (i,j,k) in current_predictions:
      distribution[i] += 1
    logging.info("Distribution of predictions by cancer: " + str(distribution))

  def number_predictions_the_same(self, current_predictions):
    #old_predictions = set([x[1] for x in self.old_predictions])
    new_predictions = set([x[1] for x in current_predictions])
    logging.info("Number of repeat predictions: " + str(len(self.old_predictions.intersection(new_predictions))))

  ###
  # Given known chemotherapeutics, predict pairs of drugs that are likely to work well together.
  ###
  def evaluate_combos(self, t):
    (chemo_drugs, cid_to_drug) = self.load_chemo_drugs()
    #self.generate_random_combos(t, chemo_drugs, cid_to_drug)
    for d in t.disease_to_index.keys():
      chemo_tuples = []
      logging.info("Combos for disease "+d)
      PU0 = t.P.U[0][t.disease_to_index[d]]
      for (cid_i, cid_j) in combinations(chemo_drugs, 2):
        c_tuple = self.score_drug_combo(t, cid_i, cid_j, PU0=PU0)
        chemo_tuples.append(c_tuple)
      chemo_tuples = sorted(chemo_tuples, key=lambda x: -x.score)
      logging.info("Top drug combos:")
      for c_tuple in chemo_tuples[:20]:
        logging.info(cid_to_drug[c_tuple.cid_i]+"\t"+cid_to_drug[c_tuple.cid_j]+"\t"+str(c_tuple.score))
      logging.info("End top drug combos")


  ###
  # Returns a list of cids of known cancer drugs from a csv.  csv must have the cids as the last element (line[-1])
  # and no header
  ###
  def load_chemo_drugs(self):
    chemo_drugs = []
    cid_to_drug = {}
    with open(self.params.datadir+'chemo_drugs.csv') as f:
      c = csv.reader(f)
      for line in c:
        cid_no = line[-1]
        cid_no = 'CID' + cid_no.rjust(9, '0')
        chemo_drugs.append(cid_no)
        cid_to_drug[cid_no] = line[0]
    return (chemo_drugs, cid_to_drug)

  ###
  # Prints 100 unique, randomly selected drug combinations of known chemo drugs which are found in the tensor.
  ###
  def generate_random_combos(self, t, chemo_drugs, cid_to_drug):
    i = 0
    random_pairs = set()
    while i < 100:
      numpy.random.shuffle(chemo_drugs)
      cids = chemo_drugs[:2]
      cids = sorted(cids)
      (cid_i, cid_j) = cids
      try:
        index_i = t.drugs_to_index[cid_i]
        index_j = t.drugs_to_index[cid_j]
      except KeyError:
        continue
      row_i = t.P.U[1][index_i]
      row_j = t.P.U[1][index_j]
      i_norm = numpy.dot(row_i, row_i)**0.5
      j_norm = numpy.dot(row_j, row_j)**0.5
      if i_norm == 0.0 or j_norm == 0.0:
        continue
      if (cid_i, cid_j) in random_pairs:
        continue
      random_pairs.add((cid_i, cid_j))
      i += 1
    print "printing random pairs"
    for p in random_pairs:
      print cid_to_drug[p[0]]+"\t"+cid_to_drug[p[1]]



  ###
  # PU0 is the factor vector for an input disease.  If no PU0 is given,
  # we do a disease-independent comparison
  # Returns a chemo_tuple with the score for that pair of drugs
  ###
  def score_drug_combo(self, t, cid_i, cid_j, PU0=None):
    try:
      index_i = t.drugs_to_index[cid_i]
      index_j = t.drugs_to_index[cid_j]
    except KeyError:
      return ChemoTuple(cid_i, cid_j, float('-inf'))
    row_i = t.P.U[1][index_i]*t.P.lmbda
    row_j = t.P.U[1][index_j]*t.P.lmbda
    i_norm = numpy.dot(row_i, row_i)**0.5
    j_norm = numpy.dot(row_j, row_j)**0.5
    ij_dot = numpy.dot(row_i, row_j)
    proj_i = numpy.dot(row_i, PU0) / (numpy.dot(PU0, PU0)**0.5)
    proj_j = numpy.dot(row_j, PU0) / (numpy.dot(PU0, PU0)**0.5)
    score = (i_norm * j_norm - numpy.abs(ij_dot)) * proj_i * proj_j
    if i_norm == 0.0 or j_norm == 0.0 or score < 0:
      return ChemoTuple(cid_i, cid_j, float('-inf'))
    score = numpy.log(i_norm * j_norm - numpy.abs(ij_dot)) + numpy.log(proj_i) + numpy.log(proj_j)
    if math.isnan(score): #this occurs when either of the projection vectors are negative.  Which means we don't want them anyhow.
      return ChemoTuple(cid_i, cid_j, float('-inf'))
    return ChemoTuple(cid_i, cid_j, score)

if __name__ == '__main__':
  print "Don't run this file.  Run tensor_evaluate."
