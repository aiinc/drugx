# Copyright 2015, 2016 Aaron Wise, Murat Can Cobanoglu
# 
# This file is part of DrugX.
# 
# DrugX is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# DrugX is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with DrugX.  If not, see <http://www.gnu.org/licenses/>.


import sys
import csv
import cPickle as pickle
import logging
import multiprocessing as mp
from tcga_analyze import AnalysisTuple
from drug_disease import create_didr
import params_parser
import chemfp.types
import chemfp.commandline.cmdsupport
import chemfp.io
import chemfp.search
import chemfp.bitops
import chemfp

###
# Used to compare drugs.  Currently uses the FP4 list of SMILES to do the comparison.
###
class DrugSimilarity(object):

  ###
  # Attributes
  #   drugs: a set of the drugs we include in our comparisons
  #   fps: our chemfp arena containing all the drugs and their fingerprints.
  #   similarities: a map of CIDs to a list of (cid, score) tuples of the top-20 nearest neighbor
  #     chemicals with > 0.7 tanimoto similarity
  ###

  def __init__(self, drugs, path='', identifier='', regenerate=False):
    logging.info('Beginning to create drug similarity object')
    self.regenerate = regenerate
    self.drugs = set(drugs)
    self.path = path
    self.identifier = identifier
    self.fpspath = self.path+self.identifier+".fps"
    self.simpath = self.path+self.identifier+"drugsim.pickle"
    if regenerate:
      self.calculate_fingerprints()
      self.load_fingerprints()
      self.calculate_similarities()
    else:
      self.load_fingerprints()
      self.load_similarities()

  ###
  # Returns the smiles corresponding to each of our drugs
  ###
  def calculate_smiles(self):
    smiles = {}
    with open(self.path+'chemicals.v4.0.tsv', 'rb') as f:
      c = csv.reader(f, delimiter='\t')
      for line in c:
        if line[0] in self.drugs:
          smiles[line[0]] = line[-1]
    return smiles

  ###
  # Given our chemicals, calculates our fingerprints and also saves them to a file
  ###
  def calculate_fingerprints(self):
    smiles = self.calculate_smiles()
    logging.info('SMILES retrieved')
    self.build_fps(smiles)

  ###
  # Uses chemfp to build an FPS file, which contains all the fingerprints for our drugs of interest.
  ###
  def build_fps(self, smi_map):
    with open('tmp.smi', 'w') as f:
      for (cid, smi) in smi_map.iteritems():
        f.write(smi+"\t"+cid+"\n")
    opener = chemfp.types.get_fingerprint_family("OpenBabel-FP4")()
    # Ready the input reader/iterator
    logging.info('reading tmp.smi')
    metadata, reader = chemfp.commandline.cmdsupport.read_multifile_structure_fingerprints(
        opener, ['tmp.smi'], format = None,
        id_tag = None, aromaticity = None, errors ='strict')
    logging.info('Writing fps')
    try:
        chemfp.io.write_fps1_output(reader, self.fpspath, metadata)
    except ParseError, err:
        sys.stderr.write("ERROR: %s. Exiting." % (err,))
        raise SystemExit(1)

  ###
  # loads our fingerprints from a file
  ###
  def load_fingerprints(self):
    self.fps = chemfp.load_fingerprints(self.fpspath)

  ###
  # Precalculates some number of high scoring similarities.
  ###
  def calculate_similarities(self):
    self.similarities = {}
    results = chemfp.search.knearest_tanimoto_search_symmetric(self.fps, k=20)
    for (query_id, hits) in zip(self.fps.ids, results):
      self.similarities[query_id] = hits.get_ids_and_scores()
    with open(self.simpath, 'wb') as f:
      pickle.dump(self.similarities, f)

  ###
  # Loads pre-existing similarities from a file.
  ###
  def load_similarities(self):
    with open(self.simpath, 'rb') as f:
      self.similarities = pickle.load(f)

  ###
  # Given two CIDs, returns their FP4 tanimoto distance.
  ###
  def get_similarity(self, cid1, cid2):
    row1 = self.fps[self.fps.get_index_by_id(cid1)]
    row2 = self.fps[self.fps.get_index_by_id(cid2)]
    return chemfp.bitops.byte_tanimoto(row1[1], row2[1])

  ###
  # Output a matrix with all-to-all drug similarities.
  ###
  def write_alltoall(self, outf='all2all.csv'):
    logging.info('Writing all-to-all drug similarity matrix')
    drugs = sorted(list(self.drugs))
    p = mp.Pool()
    with open(outf,'w') as f:
      f.write(','+','.join(drugs)+'\n')
      for drug in drugs:
        f.write(drug)
        for drug2 in drugs:
          f.write(',{:.6f}'.format(self.get_similarity(drug,drug2)))
        f.write('\n')


#TODO: We need to write a better factory for this class.
#1.  raise an exception if there are no drugs passed in and we can't find a pickle
#2.  if we pass in some drugs, and the drugs don't match the drugs in the stored drsim, recalculate it
#3.  if regen=false, but we don't have the necessary files, make sure we load it.
def create_drsim(drugs='', identifier='', regenerate=False, path=''):
  return DrugSimilarity(drugs, path, identifier=identifier, regenerate=regenerate)
  '''if regenerate:
    ds = DrugSimilarity(drugs, regenerate=True)
    with open('drsim.pickle', 'wb') as f:
      pickle.dump(ds, f)
  else:
    try:
      with open('drsim.pickle', 'rb') as f:
        ds = pickle.load(f)
    except IOError:
        ds = DrugSimilarity(drugs, regenerate=regenerate)
        with open('drsim.pickle', 'wb') as f:
          pickle.dump(ds, f)
  return ds'''

if __name__ == '__main__':
  param_in = sys.argv[1]
  params = params_parser.parse_yaml(param_in)
  logging.basicConfig(level=logging.INFO,
                      filename=params.identifier+'.log',
                      format='%(asctime)s %(name)-12s %(levelname)-8s %(message)s',
                      datefmt='%m-%d %H:%M')
  logging.info('Program start: Main in drug_similarity.py')
  didr = create_didr(params)
  ds = create_drsim(drugs=didr.drugs, identifier='v4', regenerate=False, path=params.datadir)
  print ds.fps
  ds.write_alltoall()
