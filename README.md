# README #

DrugX is written for predicting targeted cancer therapeutics using tensor factorization. 

We construct an integrated drug-target-disease tensor using transcriptomic profile data for different cancer types(RNA-seq V2 files) from the TCGA for target-disease links, and drug-target interaction data from STITCH.

We then use this drug-target-disease tensor to predict chemicals most likely to be novel antineoplastic agents against various cancer types. 

This software was written by Aaron Wise (e-mail: quejebo@gmail.com) and Murat Can Cobanoglu (e-mail: mcancobanoglu@gmail.com) in 2015 and 2016. 

The software is available under GNU Lesser General Public License. 